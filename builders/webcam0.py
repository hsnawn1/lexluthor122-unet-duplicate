
def webcam(cam_id):    
    import cv2
    cam = cv2.VideoCapture(0)
    file = open("imagetest.txt","a+")
    file. truncate(0)
    file. close()
    cv2.namedWindow("test")

    img_counter = 0

    while True:
        ret, frame = cam.read()

        if not ret:
            print("failed to grab frame")
            break
        cv2.imshow("test", frame)

        k = cv2.waitKey(1)
        if k%256 == 27:
            # ESC pressed
            print("Escape hit, closing...")
            break
        elif k%256 == 32:
            # SPACE pressed
            img_name = "opencv_frame_{}.png".format(img_counter)
            image_path ="web_image/" + img_name
            root_path = "./"
            image_path ="test_predict/" + img_name
            path = root_path + "test_predict/" + img_name
            cv2.imwrite(path, frame)
            print("{} written!".format(img_name))
            img_counter += 1
            file = open("imagetest.txt","a+")
            #file. truncate(0)
            file.write(image_path)
            file.write("\n")
            file. close()

    cam.release()

    cv2.destroyAllWindows()
