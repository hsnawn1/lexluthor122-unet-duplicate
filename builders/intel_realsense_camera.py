# Import the Library
import pyrealsense2 as rs
import cv2
import numpy as np

########################################################################################################################
## The bag file that use to playback ##
file = 'name_of_the_file.bag'

## Command for the enable the playback ##
# To enable the playback capability, use enable_device = 1
# To Disable the playback capability, use enable_device = 0
enable_device = 0

## Camera Configuration ##
# Camera Configuration (Recommended: Use the same config as the bag file or the time you record the bag file)
hres = 640
vres = 480
fps = 30

# IR_1 is the left Infrared, IR_2 is for the Right Infrared
# If ir_x = 0, then it's off. If ir_x = 1, it's on
ir_1 = 1
ir_2 = 0

# Set this align option to align the depth to rgb or keep it original FOV
fill_stereo_camera = "y"

# Set up the align camera config:
camera_align_config = "y"
########################################################################################################################

class IntelRealSenseCamera:
    # Initialize the camera
    def __init__(self):
        print("Loading Intel RealSense Camera")
        self.pipeline = rs.pipeline()

        config = rs.config()

        # Tell config that we will use a recorded device from file to be used by the pipeline through playback.
        if enable_device == 1:
            rs.config.enable_device_from_file(config, file)

        # Configure the pipeline to stream the depth stream
        # Change this parameters according to the recorded bag file resolution
        config.enable_stream(rs.stream.color, hres, vres, rs.format.rgb8, fps)
        config.enable_stream(rs.stream.depth, hres, vres, rs.format.z16, fps)
        if ir_1 == 1:
            config.enable_stream(rs.stream.infrared, 1, hres, vres, rs.format.y8, fps)
        if ir_2 == 1:
            config.enable_stream(rs.stream.infrared, 2, hres, vres, rs.format.y8, fps)

        # Start streaming from file
        self.pipeline.start(config)

        # Align the camera Stereo Camera (Depth and Infrared Sensor) to RGB Sensor, because RGB and Stereo Sensors have
        # a different FOV.
        # So we need to align the FOV to make sure that all the frame that process is the same
        if camera_align_config == "y":
            align_rgb_depth = rs.stream.color
            self.align_depth = rs.align(align_rgb_depth)
            align_rgb_infrared = rs.stream.color
            self.align_infrared = rs.align(align_rgb_infrared)

        if camera_align_config == "n":
            align_rgb_depth = rs.stream.color
            self.align_depth = align_rgb_depth
            align_rgb_infrared = rs.stream.color
            self.align_infrared = align_rgb_infrared

    # Get the Camera to Stream
    def get_frame_stream(self):
        if camera_align_config == "y":
            # Wait for a coherent pair of frames: color, depth, and infrared
            frames = self.pipeline.wait_for_frames()
            aligned_frames = self.align_depth.process(frames)
            color_frame = aligned_frames.get_color_frame()
            depth_frame = aligned_frames.get_depth_frame()

            aligned_infrared = self.align_infrared.process(frames)
            if (ir_1 == 1) & (ir_2 == 0):
                infrared_frame = aligned_infrared.get_infrared_frame(1)
            if (ir_2 == 1) & (ir_1 == 0):
                infrared_frame_2 = aligned_frames.get_infrared_frame(2)
        if camera_align_config == "n":
            # Wait for a coherent pair of frames: color, depth, and infrared
            frames = self.pipeline.wait_for_frames()
            color_frame = frames.get_color_frame()
            depth_frame = frames.get_depth_frame()
            if (ir_1 == 1) & (ir_2 == 0):
                infrared_frame = frames.get_infrared_frame(1)
            if (ir_2 == 1) & (ir_1 == 0):
                infrared_frame = frames.get_infrared_frame(2)

        if not color_frame or not depth_frame or not infrared_frame:
            # If there is no frame, probably camera not connected, return False
            print("Error!. No Camera Detected!. Make Sure That The Intel Realsense Camera Is Correctly Connected")
            return False, None, None, None, None

        # Set the number inside () to define the visualized depth color:
        # Possible values for color_scheme:
        # 0 - Jet; 1 - Classic; 2 - WhiteToBlack; 3 - BlackToWhite; 4 - Bio; 5 - Cold; 6 - Warm; 7 - Quantized; 8 - Pattern
        colorizer = rs.colorizer(2)
        # depth_color_frame = colorizer.colorize(filled_depth)

        # Setup the depth hole filler
        if fill_stereo_camera == "y":
            # Apply filter to fill the Holes in the depth image
            spatial = rs.spatial_filter()
            spatial.set_option(rs.option.holes_fill, 1)
            filtered_depth = spatial.process(depth_frame)
            filtered_infrared = spatial.process(infrared_frame)

            # Fill the Holes in the Stereo Sensor (Depth Sensor and Infrared Sensor)
            hole_filling = rs.hole_filling_filter()
            filled_depth = hole_filling.process(filtered_depth)
            filled_infrared = hole_filling.process(filtered_infrared)

            # Create colormap to show the visual depth of the Objects
            depth_color_frame = colorizer.colorize(filled_depth)
            filled_infrared = filled_infrared

        if fill_stereo_camera == "n":
            depth_color_frame = colorizer.colorize(depth_frame)
            filled_infrared = infrared_frame

        # Convert depth_frame to numpy array to render image in opencv
        color_image = np.asanyarray(color_frame.get_data())
        depth_color_image = np.asanyarray(depth_color_frame.get_data())
        infrared_image = np.asanyarray(filled_infrared.get_data())

        # Special for color_image, convert bgr (opencv)  to rgb (opencv)
        # And convert the Infrared Sensor to a 3D Matrix as the numpy can't process the 2 Channel Matrix
        color_rgb = cv2.cvtColor(color_image, cv2.COLOR_BGR2RGB)
        infrared_3d = cv2.cvtColor(infrared_image, cv2.COLOR_GRAY2RGB)

        return True, color_rgb, depth_color_image, infrared_3d

    def release(self):
        self.pipeline.stop()