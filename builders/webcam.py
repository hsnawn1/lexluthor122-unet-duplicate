from intel_realsense_camera import *
import cv2

# Load the RealSense camera
# Get the Intel RealSense Camera Frame into real time

def rescale_frame(frame, scale):    # works for image, video, live video
    width = int(frame.shape[1] * scale)
    height = int(frame.shape[0] * scale)
    dimensions = (width, height)
    return cv2.resize(frame, dimensions, interpolation=cv2.INTER_AREA)

def webcam(cam_id):
    rs = IntelRealSenseCamera()
    #rs.set(3, 1440)
    #cam = cv2.VideoCapture(0)
    file = open("imagetest.txt","a+")
    file. truncate(0)
    file. close()
    cv2.namedWindow("test")

    img_counter = 0

    while True:
        ret, frame, depth_color_image, infrared_3d = rs.get_frame_stream()
        frame = rescale_frame(frame, scale=.6)

        if not ret:
            print("failed to grab frame")
            break
        cv2.imshow("test", frame)

        k = cv2.waitKey(1)
        if k%256 == 27:
            # ESC pressed
            print("Escape hit, closing...")
            break
        elif k%256 == 32:
            # SPACE pressed
            img_name = "opencv_frame_{}.png".format(img_counter)
            image_path ="web_image/" + img_name
            root_path = "./"
            image_path ="test_predict/" + img_name
            path = root_path + "test_predict/" + img_name
            cv2.imwrite(path, frame)
            print("{} written!".format(img_name))
            img_counter += 1
            file = open("imagetest.txt","a+")
            #file. truncate(0)
            file.write(image_path)
            file.write("\n")
            file. close()

    cam.release()

    cv2.destroyAllWindows()
